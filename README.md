# Aplicacion web ecommerce **(Back)** 🚀

Estas instrucciones te permitirán preparar tu ambiente y obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas. Primero necesitamos instalar los programas y herramientas necesarias para que el sistema pueda ejecutarse de manera local sin problemas, a continuacion los pasos a seguir:

* Desacargar e instalar **NodeJS 12 o 13** y se puede descargar desde su pagina web que dejo a continuacion:
```
https://nodejs.org/en/
```
Puedes chequear la version instalada con el siguiente comando:
```
npm -v
```

* Hacer git clone de este repositorio en alguna carpeta donde desees guardarlo.
* Crear una carpeta en la raiz del proyecto llamada .env y copiar las variables que estan en el .env.example, los valores los dejo a continuacion:
```
PORT=8000
CORS=*

DB_USER=db_user_digevoecommerce
DB_PASSWORD=l5AWLo2zxoldeqqq
DB_HOST=cluster0-8pe5k.mongodb.net
DB_PORT=27017
DB_NAME=digevoecommerce_db
```


## Instalación 🔧

A continuación un par de comandos que te indican lo que debes ejecutar para tener un entorno de desarrollo ejecutandose.

* Una vez instalado necesitaremos tener en el proyectos las dependecnias y librerias usadas, para ello ejecutar el siguiente comando:
```
npm install
```
* luego para levantar nuestro proyecto debemos ejecutar el siguiente comando y la aplicacion comenzara a compilarse:
```
npm run dev
```
Si cada paso anterior se ejecuto correctamente veras en tu terminal algo como lo siguiente:
```
[nodemon] starting `node index index.js`    
Listening http://localhost:8000
```
Esto indica que el proyecto esta corriendo en el puerto 8000.

## Ejecutando las pruebas ⚙️

Este proyecto tiene pruebas unitarias para el buen desarrollo del mismo, a continuación el comando a ejecutar para ver las pruebas.

```
npm t
```
Estas pruebas demuestran que las rutas estan funcionando en perfectas condiciones, podras compribarlo porque en la terminal se debi mostrar lo siguiente:

```
  routes - api - products
    GET /products
      √ should respond with status 200 (55ms)
      √ should respond with content type json
      √ should respond with not error


  3 passing (245ms)
```

## Herramientas utilizadas 🛠️

Estas fueron las tecnologias para el desarrollo del proyecto:

* [NodeJS](https://nodejs.org/es/) - El leguaje back usado
* [Express](https://material.angular.io/) - El framework back usado
* [MongoDB](https://www.mongodb.com/) - La base de datos utilizada


## Autores ✒️

* **Yoneiker Plaza** - *Desarrollo Total*

## Expresiones de Gratitud 🎁

* De antamno muchas gracias por la oportunidad brindada 🤓.



---
⌨️ Creado por Yoneiker Plaza 😊
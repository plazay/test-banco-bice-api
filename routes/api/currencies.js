const express = require("express");
const router = express.Router();
const CurrenciesService = require("../../services/currencies");

const currenciesService = new CurrenciesService();

router.get("/", async function(req, res, next) {
  const { tags } = req.query;

  console.log("req", req.query);

  try {
    const currencies = await currenciesService.getCurrencies({ tags });

    res.send(currencies);
  } catch (err) {
    next(err);
  }
});

router.post("/", async function(req, res, next) {
  const { body: currency } = req;

  console.log("req", req.body);

  try {
    const createdCurrency = await currenciesService.createCurrency({ currency });

    res.status(201).json({
      createdCurrency,
      message: "currency created"
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;

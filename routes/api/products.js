const express = require("express");
const ProductsService = require("../../services/products");

function productsApi(app) {
  const router = express.Router();
  app.use("/api/products", router);

  const productService = new ProductsService();

  router.get("/", async function(req, res, next) {
    const { tags } = req.query;

    try {
      const products = await productService.getProducts({ tags });

      res.send(products);
    } catch (err) {
      next(err);
    }
  });

  router.get("/:id", async function(req, res, next) {
    const { id } = req.params;

    console.log("req", req.params);

    try {
      const product = await productService.getProduct({ id });

      res.send(product);
    } catch (err) {
      next(err);
    }
  });

  router.post("/", async function(req, res, next) {
    const { body: product } = req;

    console.log("req", req.body);

    try {
      const createdProduct = await productService.createProduct({ product });

      res.status(201).json({
        createdProduct,
        message: "product created"
      });
    } catch (err) {
      next(err);
    }
  });

  router.put("/:id", async function(req, res, next) {
    const { id } = req.params;
    const { body: product } = req;

    console.log("req", req.params, req.body);

    try {
      const updatedProduct = await productService.updateProduct({
        id,
        product
      });
      res.status(200).json({
        updatedProduct,
        message: "product updated"
      });
    } catch (err) {
      next(err);
    }
  });

  router.delete("/:productId", async function(req, res, next) {
    const { productId } = req.params;

    console.log("req", req.params);

    try {
      const deletedProduct = await productService.deleteProduct({ productId });

      res.status(200).json({
        deletedProduct,
        message: "product deleted"
      });
    } catch (err) {
      next(err);
    }
  });
}
module.exports = productsApi;

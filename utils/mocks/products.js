const productsMock = [
    {
        _id: "5e7e558e40f4352e388cd900",
        image: "assets/images/polera.jpg",
        title: "Camiseta",
        price: 80000,
        description: "Camiseta muy linda para el verano",
        tags: ["expensive", "brown"]
    },
    {
        _id: "5e7e7137bb288e174c92809e",
        image: "assets/images/sombrero.jpg",
        title: "Sombrero",
        price: 80000,
        description: "Sombrero de dama",
        tags: ["white", "cheap"]
    },
    {
        _id: "5e7e7234bb288e174c9280a2",
        image: "assets/images/bolso.jpg",
        title: "Bolso",
        price: 50000,
        description: "Hermoso y a la moda",
        tags: ["expensive", "black"]
    },
    {
        _id: "5e7e72a8bb288e174c9280a3",
        image: "assets/images/tasa.jpg",
        title: "Tasa de café",
        price: 20000,
        description: "Sabroso y con estilo",
        tags: ["red", "expensive"]
    },
    {
        _id: "5e7e72d7bb288e174c9280a4",
        image: "assets/images/poleron.png",
        title: "Poleron",
        price: 2000000,
        description: "Para los mas jovenes",
        tags: ["black", "expensive"]
    },
    {
        _id: "5e7e7495bb288e174c9280a6",
        image: "assets/images/notebook.jpg",
        title: "Notebook",
        price: 1000000,
        description: "notebook 14",
        tags: ["green", "cheap"]
    }
]

function filteredProductsMock(tag) {
  return productsMock.filter(product => product.tags.includes(tag));
}

class ProductsServiceMock {
  async getProducts() {
    return Promise.resolve(productsMock);
  }

  async createProduct() {
    return Promise.resolve("6bedb1267d1ca7f3053e2875");
  }
}

module.exports = {
  productsMock,
  filteredProductsMock,
  ProductsServiceMock
};
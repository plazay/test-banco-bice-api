const MongoLib = require("../lib/mongo");

class CurrenciesService {
  constructor() {
    this.collection = "currencies";
    this.mongoDB = new MongoLib();
  }

  async getCurrencies({ tags }) {
    const query = tags && { tags: { $in: tags } };
    const currencies = await this.mongoDB.getAll(this.collection, query);

    return currencies || [];

  }
  
  async createCurrency({ currency }) {
    const createCurrencyId = await this.mongoDB.create(this.collection, currency);

    return createCurrencyId;
  }

}

module.exports = CurrenciesService;

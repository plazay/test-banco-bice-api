const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');
const app = express();
const productsRouter = require('./routes/products');
const productsApiRouter = require('./routes/api/products');
const currenciesApiRouter = require('./routes/api/currencies');
const cors = require("cors");

app.use(cors());
app.use(bodyParser.json());
app.use("/static", express.static(path.join(__dirname, "public")));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use("/products", productsRouter);
productsApiRouter(app);
app.use("/api/currencies", currenciesApiRouter);

app.get("/", function(req, res) {
  res.redirect("/products");
});

app.use(bodyParser.json());

const server = app.listen(8000, function() {
  console.log(`Listening http://localhost:${server.address().port}`);
});
